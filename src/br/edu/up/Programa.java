package br.edu.up;

import java.util.List;

import br.edu.up.dao.PessoaDAO;
import br.edu.up.model.Pessoa;

public class Programa {
	
	public static void main(String[] args) throws Exception{

		//listar();
		//inserir();
		//atualizar();
		//excluir();
		
		PessoaDAO pessoaDao = new PessoaDAO();
		
		List<Pessoa> lista = pessoaDao.listar();
		for (Pessoa pessoa : lista) {
			System.out.println("Quem �? " + pessoa.getNome()); 
		}
		
		Pessoa p = new Pessoa();
		p.setNome("Jo�o JPA");
		p.setRg("1234");
		
		pessoaDao.salvar(p); 
	}
}